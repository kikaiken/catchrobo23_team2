using UnityEngine;
using MyROS;
using UnityEngine.UI;

public class FieldLeftButton : MonoBehaviour
{
    // ボタンの参照
    [SerializeField] private Button button;
    float blue = 128f;
    bool change_forward = true;
    float change_speed = 5.0f;
    // シーン読み込み時に呼び出される関数
    void Start()
    {
        //TryGetComponent(out ROSControl.Instance.ros2Unity);
        // onClickに関数を登録
        button.onClick.AddListener(OnButton);
    }
    private void Update()
    {
        if(ROSControl.Instance.field_state == 2)
        {
            Color newColor = new Color(0f, 1f, blue/256);
            button.image.color = newColor;
            if (change_forward)
            {
                blue += change_speed;
                if(blue > 256f)
                {
                    change_forward = false;
                }
            }
            else
            {
                blue -= change_speed;
                if (blue < 128f)
                {
                    change_forward = true;
                }
            }
        }
        else
        {
            button.image.color = Color.white;
        }
    }

    // ボタンが押されたときに呼び出される関数
    void OnButton()
    {
        if (ROSControl.Instance.field_state == 2)
        {
            ROSControl.Instance.field_state = 0; //初期状態に戻す
            ROSControl.Instance.linear_state = 0;
            ROSControl.Instance.interrupt_on = false;
            ROSControl.Instance.dynamixel_on = false;
            ROSControl.Instance.grip_start = false;
            ROSControl.Instance.collect_start = false;
            Debug.Log("Field左シュートButton無効化");
        }
        else
        {
            ROSControl.Instance.field_state = 2;
            ROSControl.Instance.linear_state = 0;
            ROSControl.Instance.interrupt_on = false;
            ROSControl.Instance.dynamixel_on = false;
            ROSControl.Instance.grip_start = false;
            ROSControl.Instance.collect_start = false;
            Debug.Log("Field左シュートButton有効化");
        }
    }
}