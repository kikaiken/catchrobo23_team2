using MyROS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterruptInitButton : MonoBehaviour
{
    // ボタンの参照
    [SerializeField] private Button button;
    float blue = 128f;
    bool change_forward = true;
    float change_speed = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(OnButton);
    }

    // Update is called once per frame
    void Update()
    {
        if (ROSControl.Instance.field_state == 0)
        {
            button.image.color = Color.gray;
        }
        else
        {
            if (ROSControl.Instance.interrupt_on)
            {
                Color newColor = new Color(0f, 1f, blue / 256);
                button.image.color = newColor;
                if (change_forward)
                {
                    blue += change_speed;
                    if (blue > 256f)
                    {
                        change_forward = false;
                    }
                }
                else
                {
                    blue -= change_speed;
                    if (blue < 128f)
                    {
                        change_forward = true;
                    }
                }
            }
            else
            {
                button.image.color = Color.white;
            }
        }
    }

    void OnButton()
    {
        if (!(ROSControl.Instance.field_state == 0))
        {
            if (ROSControl.Instance.interrupt_on)
            {
                ROSControl.Instance.interrupt_on = false;
                Debug.Log("妨害機構Button無効化");
            }
            else
            {
                ROSControl.Instance.interrupt_on = true;
                std_msgs.msg.Int16 msg16 = new std_msgs.msg.Int16();
                msg16.Data = 0;
                ROSControl.Instance.speak_pub.Publish(msg16);
                Debug.Log("妨害機構Button有効化");
            }
        }
    }
}