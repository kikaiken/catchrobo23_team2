using ROS2;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace MyROS
{
    public class ROSControl : MonoBehaviour
    {
        internal static readonly ROSConfig Instance = new ROSConfig();
        // Start is called before the first frame update
        void Start()
        {
            TryGetComponent(out Instance.ros2Unity);
        }

        private void Update()
        {
            if (Instance.ros2Unity.Ok())
            {
                if(Instance.ros2Node == null)
                {
                    Instance.ros2Node = Instance.ros2Unity.CreateNode("unity_node");
                    Instance.linear_vel_pub = Instance.ros2Node.CreatePublisher<geometry_msgs.msg.Twist>("/linear_vel");
                    Instance.interrupt_on_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Bool>("/interrupt_on");
                    Instance.field_pos_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Int16>("/field_pos");
                    Instance.linear_auto_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Bool>("/linear_auto");
                    Instance.hand_grip_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Bool>("/hand_grip");
                    Instance.hand_collect_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Bool>("/hand_collect");
                    Instance.dynamixel_on_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Int16>("/dynamixel_on");
                    Instance.speak_pub = Instance.ros2Node.CreatePublisher<std_msgs.msg.Int16>("/speak");
                    Instance.s0 = Instance.ros2Node.CreateSubscription<std_msgs.msg.Bool>("/linear_comp", Instance.linear_comp_callback);
                    Instance.s1 = Instance.ros2Node.CreateSubscription<std_msgs.msg.Bool>("/grip_comp", Instance.grip_comp_callback);
                    Instance.s2 = Instance.ros2Node.CreateSubscription<std_msgs.msg.Bool>("/collect_comp", Instance.collect_comp_callback);
                }
                std_msgs.msg.Int16 msg16 = new std_msgs.msg.Int16();
                std_msgs.msg.Bool msgBool = new std_msgs.msg.Bool();

                // field_pos publish
                msg16.Data = Instance.field_state;
                Instance.field_pos_pub.Publish(msg16); //0:���ݒ� 1:�V���[�^�[���E�̃t�B�[���h 2:�V���[�^�[�����̃t�B�[���h

                // interrupt_on publish
                msgBool.Data = Instance.interrupt_on;
                Instance.interrupt_on_pub.Publish(msgBool);

                // dynamixel_on publish
                if (Instance.dynamixel_on)
                {
                    if(Instance.field_state == 1) { msg16.Data = 1; } //�E�t�B�[���h�ŏ�����
                    if(Instance.field_state == 2) { msg16.Data = 2; } //���t�B�[���h�ŏ�����
                }else
                {
                    msg16.Data = 0; //�Ǐ]on
                }
                Instance.dynamixel_on_pub.Publish(msg16);

                // linear_auto publish
                if (Instance.linear_state == 1)
                {
                    msgBool.Data = true;
                }
                else
                {
                    msgBool.Data = false;
                }
                Instance.linear_auto_pub.Publish(msgBool);

                // hand_grip publish
                msgBool.Data = Instance.grip_start;
                Instance.hand_grip_pub.Publish(msgBool);

                // hand_collect publish
                msgBool.Data = Instance.collect_start;
                Instance.hand_collect_pub.Publish(msgBool);
            }
        }
    }

    class ROSConfig
    {
        public ROS2UnityComponent ros2Unity;
        public ROS2Node ros2Node;
        public IPublisher<geometry_msgs.msg.Twist> linear_vel_pub; // f7��
        public IPublisher<std_msgs.msg.Bool> interrupt_on_pub; // esp��
        public IPublisher<std_msgs.msg.Int16> field_pos_pub; //bt�� 
        public IPublisher<std_msgs.msg.Bool> linear_auto_pub; //bt��
        public IPublisher<std_msgs.msg.Bool> hand_grip_pub;
        public IPublisher<std_msgs.msg.Bool> hand_collect_pub;
        public IPublisher<std_msgs.msg.Int16> dynamixel_on_pub; // esp��
        public IPublisher<std_msgs.msg.Int16> speak_pub; //raspi��
        /* 0:�W�Q�@�\��|�� unity
         * 1:BehaviorTree�N�� bt
         * 2:
         * 3:�_�C�i�~�N�Z���N�� unity
         * 4:���[�N������� bt4
         * 5:���[�N���o bt
         * 6:�͂� bt
         * 7:��� bt
         */
        public ISubscription<std_msgs.msg.Bool> s0; // receive linear comp
        public ISubscription<std_msgs.msg.Bool> s1; // receive grip comp
        public ISubscription<std_msgs.msg.Bool> s2; // receive collect comp
        public bool linear_comp = false;
        public bool grip_start = false;
        public bool collect_start = false;
        public bool interrupt_on = false;
        public bool dynamixel_on = false;
        public short field_state = 0;
        public short linear_state = 0;

        public void linear_comp_callback(std_msgs.msg.Bool msg)
        {
            if (msg.Data)
            {
                Debug.Log("�����������");
                std_msgs.msg.Int16 msg16 = new std_msgs.msg.Int16();
                msg16.Data = 4;
                speak_pub.Publish(msg16);
                linear_comp = true;
            }
        }

        public void grip_comp_callback(std_msgs.msg.Bool msg)
        {
            if (msg.Data)
            {
                Debug.Log("�n���h����I��");
                grip_start = false;
            }
        }

        public void collect_comp_callback(std_msgs.msg.Bool msg)
        {
            if (msg.Data)
            {
                Debug.Log("���[�N�������");
                collect_start = false;
            }
        }
    }
}