using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyROS;

public class LinearManualButton : MonoBehaviour
{
    // ボタンの参照
    [SerializeField] private Button button;
    float blue = 128f;
    bool change_forward = true;
    float change_speed = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(OnButton);
    }

    // Update is called once per frame
    void Update()
    {
        if (ROSControl.Instance.field_state == 0)
        {
            button.image.color = Color.gray;
        }
        else
        {
            if (ROSControl.Instance.linear_state == 1)
            {
                Color newColor = new Color(0f, 1f, blue / 256);
                button.image.color = newColor;
                if (change_forward)
                {
                    blue += change_speed;
                    if (blue > 256f)
                    {
                        change_forward = false;
                    }
                }
                else
                {
                    blue -= change_speed;
                    if (blue < 128f)
                    {
                        change_forward = true;
                    }
                }
            }
            else
            {
                button.image.color = Color.white;
            }
        }
    }

    void OnButton()
    {
        if (!(ROSControl.Instance.field_state == 0))
        {
            if (ROSControl.Instance.linear_state == 1)
            {
                ROSControl.Instance.linear_state = 0; //初期状態に戻す
                ROSControl.Instance.grip_start = false;
                ROSControl.Instance.collect_start = false;
                Debug.Log("Linear手動Button無効化");
            }
            else
            {
                ROSControl.Instance.linear_state = 1;
                Debug.Log("Linear手動Button有効化");
            }
        }
    }
}
