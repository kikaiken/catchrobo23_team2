using UnityEngine;
using UnityEngine.InputSystem;
using MyROS;
using UnityEngine.UI;

public class JoyController : MonoBehaviour
{
    public Color activeColor;
    public Color inactiveColor;
    private Image objectRenderer;
    // シーン読み込み時に呼び出される関数
    void Start()
    {
        objectRenderer = GetComponent<Image>();
        objectRenderer.color = inactiveColor;
        //TryGetComponent(out ROSControl.Instance.ros2Unity);
    }

    // フレーム更新時に呼び出される関数
    void Update()
    {
        var current = Gamepad.current;
        var input_vec = current.leftStick.ReadValue();
        if (ROSControl.Instance.linear_state == 1 && !ROSControl.Instance.collect_start && !ROSControl.Instance.grip_start)
        {
            objectRenderer.color = activeColor;
            if (ROSControl.Instance.ros2Unity.Ok())
            {
                geometry_msgs.msg.Twist msg = new geometry_msgs.msg.Twist();
                msg.Linear.X = input_vec.x;
                msg.Linear.Y = input_vec.y;
                ROSControl.Instance.linear_vel_pub.Publish(msg);
            }
        }
        else
        {
            objectRenderer.color = inactiveColor;
        }
    }
}
