using MyROS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatchButton : MonoBehaviour
{
    // ボタンの参照
    [SerializeField] private Button button;
    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(OnButton);
    }

    // Update is called once per frame
    void Update()
    {
        if(ROSControl.Instance.linear_state == 1)
        {
            button.enabled = true;
            if (ROSControl.Instance.grip_start)
            {
                button.image.color = Color.yellow;
            }
            else
            {
                button.image.color = Color.white;
            }
        }
        else
        {
            button.enabled = false;
            button.image.color = Color.gray;
        }
    }

    void OnButton()
    {
        ROSControl.Instance.grip_start = true;
        ROSControl.Instance.collect_start = false;
        Debug.Log("掴むButtonが押されました");
    }
}
