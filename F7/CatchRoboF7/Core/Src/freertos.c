/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <uxr/client/transport.h>
#include <rmw_microxrcedds_c/config.h>
#include <rmw_microros/rmw_microros.h>
#include <rosidl_runtime_c/string_functions.h>
#include "usart.h"
#include <std_msgs/msg/bool.h>
#include <geometry_msgs/msg/twist.h>
#include <catchrobo_msgs/srv/linear.h>
#include "CAN_Main.h"
#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){printf("Failed status on line %d: %d. Aborting.\n",__LINE__,(int)temp_rc);}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){printf("Failed status on line %d: %d. Continuing.\n",__LINE__,(int)temp_rc);}}

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
rcl_publisher_t robot_start_pub;
//rcl_publisher_t linear_state_pub;
rcl_init_options_t init_options;
rcl_allocator_t allocator;
rclc_support_t support;
rcl_node_t node;
rcl_service_t linear_service;
rcl_subscription_t linear_vel_sub;
rcl_subscription_t air_on_sub;
rclc_executor_t executor;
geometry_msgs__msg__Twist linear_vel_data;
std_msgs__msg__Bool air_on_data;
std_msgs__msg__Bool robot_start_data;
std_msgs__msg__Bool box_on_data;
geometry_msgs__msg__Twist linear_state_data;
catchrobo_msgs__srv__Linear_Request linear_req;
catchrobo_msgs__srv__Linear_Response linear_res;
rcl_publisher_t box_on_pub;
MCMD_HandleTypedef linear_arm[3];//[3];
CAN_Device air_device;
MCMD_Feedback_Typedef feedback_data[3];
float epsilon = 0.03; // [m] = 2cm
bool position_now = true;
//float diff_list[] = {0.869/2, 0.960/2, 0.749/2};//x0, y, x1 キャリブレーションの位置から原点までの補正値
/* USER CODE END Variables */
/* Definitions for F7NodeTask */
osThreadId_t F7NodeTaskHandle;
uint32_t F7TaskBuffer[ 30000 ];
osStaticThreadDef_t F7TaskControlBlock;
const osThreadAttr_t F7NodeTask_attributes = {
  .name = "F7NodeTask",
  .cb_mem = &F7TaskControlBlock,
  .cb_size = sizeof(F7TaskControlBlock),
  .stack_mem = &F7TaskBuffer[0],
  .stack_size = sizeof(F7TaskBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
bool cubemx_transport_open(struct uxrCustomTransport * transport);
bool cubemx_transport_close(struct uxrCustomTransport * transport);
size_t cubemx_transport_write(struct uxrCustomTransport* transport, const uint8_t * buf, size_t len, uint8_t * err);
size_t cubemx_transport_read(struct uxrCustomTransport* transport, uint8_t* buf, size_t len, int timeout, uint8_t* err);

void * microros_allocate(size_t size, void * state);
void microros_deallocate(void * pointer, void * state);
void * microros_reallocate(void * pointer, size_t size, void * state);
void * microros_zero_allocate(size_t number_of_elements, size_t size_of_element, void * state);
void air_on_cb(const void *msgin);
void linear_vel_cb(const void *msgin);
void linear_cb(const void *request, void *response);
//void timer_cb(rcl_timer_t* timer, int64_t last_call_time);
void timer_cb();
/* USER CODE END FunctionPrototypes */

void StartF7NodeTask(void *argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of F7NodeTask */
  F7NodeTaskHandle = osThreadNew(StartF7NodeTask, NULL, &F7NodeTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartF7NodeTask */
/**
  * @brief  Function implementing the F7NodeTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartF7NodeTask */
void StartF7NodeTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN StartF7NodeTask */
  printf("enter f7 node task\r\n");

  air_device.node_type = NODE_AIR; //エアシリンダ基盤であることを示す
  air_device.node_id = 0; //基板の番号
  for(uint8_t i=PORT_1; i<=PORT_3; i++){  //すべてのポートを初期化しないとAir基板は動かない
    air_device.device_num = i; // (i番ポートを指定)
    AirCylinder_Init(&air_device, AIR_OFF);
    HAL_Delay(10);  // このdelayは必要
  }
	for(int i = 0; i < 3; i++){
		air_device.device_num = i;
		AirCylinder_SendOutput(&air_device, AIR_ON);
	}


  // 0:small x, 2:large x, 1:y
  for(int i=0;i<3;i++){
	  osDelay(2000);
	  linear_arm[i].device.node_type = NODE_MCMD4;  // 動かす基板の種類(MCMD1とかMCMD3とか)
	  if(i == 1){
		  linear_arm[i].device.node_id = 0;  // 基板の番号(1)
		  linear_arm[i].device.device_num = 0;
	  }else if(i == 2 || i == 0){
		  linear_arm[i].device.node_id = 2;  // 基板の番号(1)
		  if(i == 0){
			  linear_arm[i].device.device_num = 1;
		  }else if(i == 2){
			  linear_arm[i].device.device_num = 0;
		  }
	  }
	  linear_arm[i].ctrl_param.ctrl_type = MCMD_CTRL_POS; // DUTY制御(POS:位置制御,VEL:速度制御)
	  linear_arm[i].ctrl_param.accel_limit = ACCEL_LIMIT_ENABLE; // 加速制限を掛ける(DISABLE:かけない)
	  linear_arm[i].ctrl_param.accel_limit_size = 2.0f; // 加速制限の値
	  linear_arm[i].ctrl_param.feedback = MCMD_FB_ENABLE;  // メインのマイコンへ情報を送信するか否か(DISABLE:かけない)
	  linear_arm[i].fb_type = MCMD_FB_POS;  // メインのマイコンへ送信する情報の種類(POS:位置,VEL:速度,DUTY:duty比)
	  linear_arm[i].ctrl_param.PID_param.kp = 6.0f;  // PIDのp項
	  linear_arm[i].ctrl_param.PID_param.ki = 0.0f;  // PIDのi項
	  linear_arm[i].ctrl_param.PID_param.kd = 1.0f;  // PIDのd項
	  linear_arm[i].ctrl_param.PID_param.kff =0.0f; //フィードフォワード制御のためのパラメーター
	  linear_arm[i].ctrl_param.gravity_compensation=GRAVITY_COMPENSATION_DISABLE; //重力補償をonにするか否か
	  linear_arm[i].ctrl_param.gravity_compensation_gain=0.0f; //重力補正の定数、詳しくはMCMD_CAN.cのCANMotor_Updateを参照
	  if(i == 1){
		  linear_arm[i].enc_dir = MCMD_DIR_FW;  //　エンコーダーの向き(FW:順回転,BC:逆回転)
		  linear_arm[i].rot_dir = MCMD_DIR_FW;  // モーターの回転の向き(FW:順回転,BC:逆回転)
	  }else if(i == 2){
		  linear_arm[i].enc_dir = MCMD_DIR_FW;  //　エンコーダーの向き(FW:順回転,BC:逆回転)
		  linear_arm[i].rot_dir = MCMD_DIR_BC;  // モーターの回転の向き(FW:順回転,BC:逆回転)
	  }else if(i == 0){
		  linear_arm[i].enc_dir = MCMD_DIR_BC;  //　エンコーダーの向き(FW:順回転,BC:逆回転)
		  linear_arm[i].rot_dir = MCMD_DIR_BC;  // モーターの回転の向き(FW:順回転,BC:逆回転)
	  }
	  linear_arm[i].limit_sw_type = LIMIT_SW_NC; // リミットスイッチの種類(no: normally open, nc: normally closed)
	  linear_arm[i].calib = CALIBRATION_ENABLE;  // キャリブレーション(初期の位置合わせ)を行うか
	  if(i == 2){
		  linear_arm[i].calib_duty = 0.4f;
	  }else{
		  linear_arm[i].calib_duty = -0.4f;  // キャリブレーション時のモーターに掛けるduty
	  }
	  linear_arm[i].offset = 0; // キャリブレーションされた位置の値(オフセットを掛ける)
	  if(i == 1){
		  linear_arm[i].quant_per_unit =  0.960/8990.75f; //ここをうまく設定してmm単位にする
	  }else if(i == 2){
		  linear_arm[i].quant_per_unit =  0.749/8595.0f; //ここをうまく設定してmm単位にする
	  }else if(i == 0){
		  linear_arm[i].quant_per_unit =  0.869/7992.25f; //ここをうまく設定してmm単位にする
	  }

	  //mcmdの初期化
	  MCMD_init(&linear_arm[i]);
	  osDelay(20);
	  MCMD_Calib(&linear_arm[i]);
	  osDelay(20);
	  MCMD_SetTarget(&linear_arm[i], 0.0);
	  osDelay(20);
	  MCMD_Control_Enable(&linear_arm[i]);
	  osDelay(20);
	  while(1){
		  feedback_data[i] = Get_MCMD_Feedback(&(linear_arm[i].device));
		  printf("feedback: %f", feedback_data[i].value);
		  if(-epsilon < feedback_data[i].value && feedback_data[i].value < epsilon){
			  printf("break");
			  break;
		  }
		  osDelay(50);
	  }
  }
  // micro-ROS configuration
  rmw_uros_set_custom_transport(true, (void *) &huart3, cubemx_transport_open, cubemx_transport_close, cubemx_transport_write, cubemx_transport_read);
  rcl_allocator_t freeRTOS_allocator = rcutils_get_zero_initialized_allocator();
  freeRTOS_allocator.allocate = microros_allocate;
  freeRTOS_allocator.deallocate = microros_deallocate;
  freeRTOS_allocator.reallocate = microros_reallocate;
  freeRTOS_allocator.zero_allocate =  microros_zero_allocate;
  if (!rcutils_set_default_allocator(&freeRTOS_allocator)) {
    printf("Error on default allocators (line %d)\n", __LINE__);
  }

  osDelay(1000);
  // micro-ROS setup
  init_options = rcl_get_zero_initialized_init_options();
  allocator = rcl_get_default_allocator();
  node = rcl_get_zero_initialized_node();
  linear_service = rcl_get_zero_initialized_service();
  executor = rclc_executor_get_zero_initialized_executor();

  RCCHECK(rcl_init_options_init(&init_options, allocator));
  osDelay(10);
  RCCHECK(rcl_init_options_set_domain_id(&init_options, 123));
  osDelay(10);
  RCCHECK(rclc_support_init_with_options(&support, 0, NULL, &init_options, &allocator));
  osDelay(10);
  RCCHECK(rclc_node_init_default(&node, "f7_node", "", &support));
  osDelay(10);
  RCCHECK(rclc_publisher_init_default(&robot_start_pub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Bool), "/robot_start"));
  osDelay(10);
  //RCCHECK(rclc_publisher_init_default(&linear_state_pub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(geometry_msgs, msg, Twist), "/linear_state"));
  //osDelay(10);
  RCCHECK(rclc_subscription_init_default(&linear_vel_sub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(geometry_msgs, msg, Twist), "/linear_vel"));
  osDelay(10);
  RCCHECK(rclc_subscription_init_default(&air_on_sub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Bool), "/air_on"));
  osDelay(10);
  RCCHECK(rclc_service_init_default(&linear_service, &node, ROSIDL_GET_SRV_TYPE_SUPPORT(catchrobo_msgs, srv, Linear), "/linear_service"));
  osDelay(10);
  RCCHECK(rclc_publisher_init_default(&box_on_pub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Bool), "/box_on"));
  osDelay(10);
  RCCHECK(rclc_executor_init(&executor, &support.context, 4, &allocator));
  osDelay(10);
  RCCHECK(rclc_executor_add_service(&executor, &linear_service, &linear_req, &linear_res, &linear_cb));
  osDelay(10);
  RCCHECK(rclc_executor_add_subscription(&executor, &air_on_sub, &air_on_data, &air_on_cb, ON_NEW_DATA));
  osDelay(10);
  RCCHECK(rclc_executor_add_subscription(&executor, &linear_vel_sub, &linear_vel_data, &linear_vel_cb, ON_NEW_DATA));
  //RCCHECK(rclc_executor_add_timer(&executor, &timer));
  /* Infinite loop */
  for(;;){
	  RCCHECK(rclc_executor_spin_some(&executor, RCL_MS_TO_NS(100)));
	  timer_cb();
  }
  RCCHECK(rcl_service_fini(&linear_service, &node));
  RCCHECK(rcl_node_fini(&node));
  /* USER CODE END StartF7NodeTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void air_on_cb(const void *msgin){
	std_msgs__msg__Bool *msg = (std_msgs__msg__Bool *)msgin;
	printf("air_on\r\n");
	air_device.node_type = NODE_AIR; //エアシリンダ基盤であることを示す
	air_device.node_id = 0; //基板の番号
	if (msg->data){
		printf("data is true");
		for(int i = 0; i < 3; i++){
			air_device.device_num = i;
			osDelay(10);
			AirCylinder_Init(&air_device, AIR_ON);
			osDelay(10);
			AirCylinder_SendOutput(&air_device, AIR_ON);
		}
	}else{
		printf("data is false");
		for(int i = 0; i < 3; i++){
			air_device.device_num = i;
			osDelay(10);
			AirCylinder_Init(&air_device, AIR_OFF);
			osDelay(10);
			AirCylinder_SendOutput(&air_device, AIR_OFF);
		}
	}
}
void linear_vel_cb(const void *msgin){
	if(position_now){
		//osDelay(2000);
		for(int i = 0; i < 3; i++){
			//変更できていない
			MCMD_Control_Disable(&linear_arm[i]);
			osDelay(20);
			linear_arm[i].ctrl_param.ctrl_type = MCMD_CTRL_VEL;
			linear_arm[i].calib = CALIBRATION_DISABLE;
			MCMD_ChangeControl(&linear_arm[i]);
			osDelay(20);
			//MCMD_Calib(&linear_arm[i]);
			//osDelay(20);
			MCMD_SetTarget(&linear_arm[i], 0.0);
			osDelay(20);
			MCMD_Control_Enable(&linear_arm[i]);
			osDelay(50);
		}
		position_now = false;
		printf("velocity control on!");
	}
	geometry_msgs__msg__Twist *msg = (geometry_msgs__msg__Twist *)msgin;
	float set_vel[3] = {(float)msg->linear.x/2.0, (float)msg->linear.y, (float)msg->linear.x/2.0};
	for(int i = 0; i < 3; i++){
		MCMD_SetTarget(&linear_arm[i], set_vel[i]*0.01);
	}
}
void linear_cb(const void *request, void *response){
	if(!position_now){
		//osDelay(2000);
		for(int i = 0; i < 3; i++){
			MCMD_Control_Disable(&linear_arm[i]);
			osDelay(20);
			linear_arm[i].ctrl_param.ctrl_type = MCMD_CTRL_POS;
			linear_arm[i].calib = CALIBRATION_DISABLE;
			MCMD_ChangeControl(&linear_arm[i]);
			osDelay(20);
			//MCMD_Calib(&linear_arm[i]);
			//osDelay(20);
			MCMD_Control_Enable(&linear_arm[i]);
			osDelay(50);
		}
		position_now = true;
		printf("position control on!");
	}
	catchrobo_msgs__srv__Linear_Request *_req = (catchrobo_msgs__srv__Linear_Request *)request;
	catchrobo_msgs__srv__Linear_Response *_res = (catchrobo_msgs__srv__Linear_Response *)response;
	float set_pos[3] = {(float)(_req->x0/1000.0), (float)(_req->y/1000.0), (float)(_req->x1/1000.0)};
	for(int i = 0; i < 3; i++){
		feedback_data[i] = Get_MCMD_Feedback(&(linear_arm[i].device));
		if(-epsilon > feedback_data[i].value - set_pos[i] || feedback_data[i].value - set_pos[i] > epsilon){
			MCMD_SetTarget(&linear_arm[i], set_pos[i]);
		}
	}
//無限ループになっている
	while(1){
		int count = 0;
		bool flag = false;
		for(int i = 0; i < 3; i++){
			//printf("%f", set_pos[i]);
			feedback_data[i] = Get_MCMD_Feedback(&(linear_arm[i].device));
			printf("%f\r\n", feedback_data[i].value - set_pos[i]);
			if(-epsilon < feedback_data[i].value - set_pos[i] && feedback_data[i].value - set_pos[i]  < epsilon){
				count ++;
				if(count == 3){
					flag = true;
				}
			}
		}
		if(flag){
			break;
		}
		osDelay(50);
	}
	_res->complete = true;
}

//void timer_cb(rcl_timer_t* timer, int64_t last_call_time){
//	RCLC_UNUSED(last_call_time);
void timer_cb(){
	printf("timer\r\n");
	robot_start_data.data = true;
	RCSOFTCHECK(rcl_publish(&robot_start_pub, &robot_start_data, NULL));
	for(int i = 0; i < 3; i++){
		feedback_data[i] = Get_MCMD_Feedback(&(linear_arm[i].device));
	}
	//linear_state_data.linear.x = (double)(feedback_data[0].value + feedback_data[1].value);
	//linear_state_data.linear.y = (double)feedback_data[2].value;
	//RCSOFTCHECK(rcl_publish(&linear_state_pub, &linear_state_data, NULL));
	//リミットスイッチ読み取り部分を記述 //PE5
	if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)==GPIO_PIN_SET){
		box_on_data.data = true;
	}else{
		box_on_data.data = false;
	}
	RCSOFTCHECK(rcl_publish(&box_on_pub, &box_on_data, NULL));
}
/* USER CODE END Application */

