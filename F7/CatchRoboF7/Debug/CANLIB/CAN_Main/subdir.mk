################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CANLIB/CAN_Main/CAN_Main.c 

OBJS += \
./CANLIB/CAN_Main/CAN_Main.o 

C_DEPS += \
./CANLIB/CAN_Main/CAN_Main.d 


# Each subdirectory must supply rules for building sources it contributes
CANLIB/CAN_Main/%.o CANLIB/CAN_Main/%.su CANLIB/CAN_Main/%.cyclo: ../CANLIB/CAN_Main/%.c CANLIB/CAN_Main/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F767xx -c -I../Core/Inc -I../micro_ros_stm32cubemx_utils/microros_static_library_ide/libmicroros/include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1 -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -I../USB_DEVICE/App -I../USB_DEVICE/Target -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/ST/STM32_USB_Device_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -I"/home/humble/Documents/VScode/catchrobo23_team2/F7/CatchRoboF7/CANLIB/CAN_Main" -I"/home/humble/Documents/VScode/catchrobo23_team2/F7/CatchRoboF7/CANLIB/Defines/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-CANLIB-2f-CAN_Main

clean-CANLIB-2f-CAN_Main:
	-$(RM) ./CANLIB/CAN_Main/CAN_Main.cyclo ./CANLIB/CAN_Main/CAN_Main.d ./CANLIB/CAN_Main/CAN_Main.o ./CANLIB/CAN_Main/CAN_Main.su

.PHONY: clean-CANLIB-2f-CAN_Main

