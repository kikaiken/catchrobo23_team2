#include <micro_ros_arduino.h>

#include <IcsHardSerialClass.h>
#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <catchrobo_msgs/srv/hand.h>
#include <std_msgs/msg/int16.h>
#include <std_msgs/msg/bool.h>

#define MODE 0x00 //状態遷移用サーボ
#define TURN 0x01 //回転用サーボ
#define LNRZ 0x02 //Z軸用サーボ
#define INTR 0x03 //妨害機構用サーボ
#define LED_PIN 2 //動作確認用LED

#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){error_loop();}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){}}

#if !defined(ESP32) && !defined(TARGET_PORTENTA_H7_M7) && !defined(ARDUINO_NANO_RP2040_CONNECT) && !defined(ARDUINO_WIO_TERMINAL)
#error This example is only available for Arduino Portenta, Arduino Nano RP2040 Connect, ESP32 Dev module and Wio Terminal
#endif

std_msgs__msg__Bool interrupt_on_data;
std_msgs__msg__Int16 field_pos_data;
catchrobo_msgs__srv__Hand_Request hand_req;
catchrobo_msgs__srv__Hand_Response hand_res;

rclc_executor_t executor;
rcl_init_options_t init_options;
rcl_allocator_t allocator;
rclc_support_t support;
rcl_node_t node;

//rcl_subscription_t interrupt_on_sub;
rcl_subscription_t field_pos_sub;
rcl_service_t hand_service;
rcl_node_options_t node_ops;

const byte EN_PIN = 5;//受信送信切り替え用のピン指定
const long BAUDRATE = 115200;//通信速度(サーボと合わせる必要あり)
const int TIMEOUT = 200;    //通信確認のタイムアウト
IcsHardSerialClass B3M(&Serial2,EN_PIN,BAUDRATE,TIMEOUT);  //インスタンス＋ENピンおよびUARTの指定

int deg; //deg:目標角度の100倍の整数値を渡す。例 30度→deg=3000 -30度→-3000。degは0±32000(±320度)を取る
int epsilon = 500;
int field_state = 0;

/*
void interrupt_on_cb(const void *msgin){
  std_msgs__msg__Bool *msg = (std_msgs__msg__Bool *)msgin;
  if(msg->data){
    if(field_state == 1){
      deg = B3M.setPos(INTR, 80000);
    }else if(field_state == 2){
      deg = B3M.setPos(INTR, -80000);
    }
  }else{
    if(field_state == 1){
      deg = B3M.setPos(INTR, 60000);
    }else if(field_state == 2){
      deg = B3M.setPos(INTR, -60000);
    }else{
      deg = B3M.setPos(INTR, 0);
    }
  }
}
*/

void field_pos_cb(const void *msgin){
  std_msgs__msg__Int16 *msg = (std_msgs__msg__Int16 *)msgin;
  field_state = msg->data;
  if(field_state == 1){
    deg = B3M.setPos(INTR, 60000);
  }else if(field_state == 2){
    deg = B3M.setPos(INTR, -60000);
  }else{
    deg = B3M.setPos(INTR, 0);
  }
}

void hand_cb(const void *request, void *response){
  catchrobo_msgs__srv__Hand_Request *_req = (catchrobo_msgs__srv__Hand_Request *)request;
  catchrobo_msgs__srv__Hand_Response *_res = (catchrobo_msgs__srv__Hand_Response *)response;
  bool move_z = false;
  bool move_mode = false;
  bool move_turn = false;
  // -320 ~ 320
  if(-320.0 < _req->z && _req->z < 320.0){
    deg = B3M.setPos(LNRZ, (int)(_req->z*100));
    move_z = true;
  }
  if(-320.0 < _req->mode && _req->mode < 320.0){
    deg = B3M.setPos(MODE, (int)(_req->mode*100));
    move_mode = true;
  }
  if(-320.0 < _req->turn && _req->turn < 320.0){
    deg = B3M.setPos(TURN, (int)(_req->turn*100));
    move_turn = true;
  }
  while(1){
    deg = B3M.getPos(MODE);
    if(abs(deg - _req->mode*100) < epsilon || !move_mode){
      deg = B3M.getPos(TURN);
      if(abs(deg - _req->turn*100) < epsilon || !move_turn){
        deg = B3M.getPos(LNRZ);
        if(abs(deg - _req->z*100) < epsilon || !move_z){
          break;
        }
      }
    }
    delay(100);
  }
  _res->complete = true;
}

// エラー発生でLEDが点滅
void error_loop(){
  delay(1000);
  printf("\renter error_loop\r\n");
  while(1){
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));
    delay(100);
  }
}

void setup() {
  // kondo setup
  set_microros_wifi_transports("KU_Kikaiken", "68025137", "192.168.0.101", 8888); //espは2.4GHzのみ対応
  pinMode(LED_PIN, OUTPUT);
  B3M.begin();//通信の設定初期化
  delay(500);
  B3M.b3m_init(0xff);//idを入れて動作を位置制御で初期化。
  
  //node
  allocator = rcl_get_default_allocator();
  init_options = rcl_get_zero_initialized_init_options();
  hand_service = rcl_get_zero_initialized_service();
  executor = rclc_executor_get_zero_initialized_executor();
  RCCHECK(rcl_init_options_init(&init_options, allocator));
  RCCHECK(rcl_init_options_set_domain_id(&init_options, 123));
  RCCHECK(rclc_support_init_with_options(&support, 0, NULL, &init_options, &allocator));
  RCCHECK(rclc_node_init_default(&node, "esp_kondo_node", "", &support));
  RCCHECK(rclc_subscription_init_default(&field_pos_sub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int16), "/field_pos"));
  //RCCHECK(rclc_subscription_init_default(&interrupt_on_sub, &node, ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int16), "/interrupt_on"));
  RCCHECK(rclc_service_init_default(&hand_service, &node, ROSIDL_GET_SRV_TYPE_SUPPORT(catchrobo_msgs, srv, Hand), "/hand_service"));
  RCCHECK(rclc_executor_init(&executor, &support.context, 2, &allocator));
  RCCHECK(rclc_executor_add_subscription(&executor, &field_pos_sub, &field_pos_data, &field_pos_cb, ON_NEW_DATA));
  //RCCHECK(rclc_executor_add_subscription(&executor, &interrupt_on_sub, &interrupt_on_data, &interrupt_on_cb, ON_NEW_DATA));
  RCCHECK(rclc_executor_add_service(&executor, &hand_service, &hand_req, &hand_res, &hand_cb));

  digitalWrite(LED_PIN, HIGH);
  printf("\rfinish setup\r\n");
}

void loop() {
  RCCHECK(rclc_executor_spin_some(&executor, RCL_MS_TO_NS(100)));
}
