//以下コピペ
#include <IcsHardSerialClass.h>
const byte EN_PIN = 5;//受信送信切り替え用のピン指定
const long BAUDRATE = 115200;//通信速度(サーボと合わせる必要あり)
const int TIMEOUT = 200;    //通信確認のタイムアウト
IcsHardSerialClass B3M(&Serial2,EN_PIN,BAUDRATE,TIMEOUT);  //インスタンス＋ENピンおよびUARTの指定
//ここまで

int i = 0;
byte id = 0x00;
int eps = 50;//許容誤差(0.5do)
int maxvalue = 9000;
int minvalue = -9000;

void setup() {
  Serial.begin(9600);
  Serial.print(B3M.begin());//通信の設定初期化
  delay(500);
  B3M.b3m_init(0xff);//idを入れて動作を位置制御で初期化。
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
Serial.println(B3M.getID());//ID取得表示(単一接続でのみ可能)

B3M.setPos(id,maxvalue);//id,角度(deg*100)

while(abs(B3M.getPos(id)-maxvalue) >eps){//指定した誤差に収まるまで待つ
  delay(100);
  Serial.println(B3M.getPos(id));
  Serial.println("waiting...max");
}

B3M.setPos(id,minvalue);//id,角度(deg*100)

while(abs(B3M.getPos(id)-minvalue) >eps){//指定した誤差に収まるまで待つ
  delay(100);
  Serial.println(B3M.getPos(id));
  Serial.println("waiting...min");
}

i++;
if(i==10){//10loopで停止
  B3M.setFree(id);//idを脱力 復帰はsetNormal(id);
}
}
