#ifndef __b3m_Base_Servo_h__
#define __b3m_Base_Servo_h__
#include "Arduino.h"

class b3mBaseClass
{
public:
	 //サーボID範囲 ////////////////////////////////////
  static constexpr int MAX_ID = 31;   ///< サーボIDの最大値
  static constexpr int MIN_ID = 0;    ///< サーボIDの最小値

  //サーボ最大最小リミット値

  static constexpr int MAX_POS = 11500;  ///<サーボのポジション最大値

  static constexpr int MIN_POS = 3500;   ///<サーボのポジション最小値
  
  static constexpr int ICS_FALSE = -1;  ///< 通信等々で失敗したときの値

  //固定値(非公開分)
  protected :

  static constexpr float ANGLE_F_FALSE = 9999.9; ///< 角度計算時、範囲内に入ってない場合は999.9にする(負側の場合はマイナスをつける)
  static constexpr int   ANGLE_I_FALSE = 0x7FFF; ///< 角度計算時、範囲内に入ってない場合は0x7FFFにする(負側の場合はマイナスをつける)

  //各パラメータ設定範囲
  static constexpr int MAX_127 = 127;   ///< パラメータの最大値
   
  static constexpr int MAX_63 = 63;     ///< パラメータの最大値(電流値)

  static constexpr int MIN_1 = 1;       ///< パラメータの最小値

  //  static const float MAX_DEG = 135.0;
  static constexpr float MAX_DEG = 180.0; ///< 角度の最大値

  //  static const float MIN_DEG = -135.0; 
  static constexpr float MIN_DEG = -180.0;  ///< 角度の最大値

  //static const int MAX_100DEG = 13500;
  static constexpr int MAX_100DEG = 18000;  ///< 角度(x100)の最大値
  
  //static const int MIN_100DEG = -13500;
  static constexpr int MIN_100DEG = -18000; ///< 角度(x100)の最小値
  
	public:
	//データ送受信 /////////////////////////////////////////////////////////////////////////////////////////////
	/**
	* @brief ICS通信の送受信
	* @param[in,out] *txBuf
	* @param[in] txLen
	* @param[out] *rxBuf 受信格納バッファ
	* @param[in] rxLen  受信データ数
	* @retval true 通信成功
	* @retval false 通信失敗
	* @attention 送信データ数、受信データ数はコマンドによって違うので注意する
	* @attention この関数は外部に書く事
	**/
     virtual bool synchronize(byte *txBuf, byte txLen, byte *rxBuf, byte rxLen);
   
  //servo関連
  private:
      int WriteCmd(byte id ,byte TxData,byte Address);
      int ReadCmd(byte id,byte Address,byte *rxCmd,byte rxLen);
  public:
      bool b3m_init(byte id);
   //サーボ位置決め設定
      int setPos(byte id,unsigned int pos);
      int setPos(byte id,unsigned int pos,unsigned int Time);    //目標値設定
      bool setFree(byte id);    //サーボ脱力
      bool setNormal(byte id);  //サーボトルク戻す
      //各種パラメータ書込み
      int setStrc(byte id, unsigned int strc);    //ストレッチ書込 1～127  1(弱）  <=>    127(強）
      int setSpd(byte id, unsigned int spd);      //スピード書込   1～127  1(遅い) <=>    127(速い)
      int setCur(byte id, unsigned int curlim);   //電流制限値書込 1～63   1(低い) <=>    63 (高い)
      int setTmp(byte id, unsigned int tmplim);   //温度上限書込   1～127  127(低温） <=> 1(高温) 
      //各種パラメータ読込み
      int getStrc(byte id);  //ストレッチ読込    1～127  1(弱） <=>     127(強）
      int getSpd(byte id);   //スピード読込      1～127  1(遅い)<=>     127(速い)
      int getCur(byte id);   //電流値読込        63←0 | 64→127
      int getTmp(byte id);   //現在温度読込      127(低温）<=>　0(高温)
      int getPos(byte id);   //現在位置読込　    ※ICS3.6以降で有効

      int getID();
      int setID(byte id);
};

#endif
