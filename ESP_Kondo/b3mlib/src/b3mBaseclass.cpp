#include "b3mBaseclass.h"

//RAM書き込み
/*
* @brief 
* @param id 
* @param txData 
* @param Address 書き込み対象のアドレス
*/
int b3mBaseClass::WriteCmd(byte id ,byte TxData,byte Address){

  byte txCmd[8];
  byte rxCmd[5];
  unsigned int reData;
  bool flg;

  txCmd[0] = (byte)(0x08);
  txCmd[1] = (byte)(0x04);
  txCmd[2] = (byte)(0x00);
  txCmd[3] = (byte)(id);
  txCmd[4] = (byte)(TxData);
  txCmd[5] = (byte)(Address);
  txCmd[6] = (byte)(0x01);
  txCmd[7] = (byte)(0x00);

  for(int i = 0 ; i<7; i++){
    txCmd[7] +=txCmd[i];
    //Serial.println(txCmd[i]);
  }
  txCmd[7] = (byte)(txCmd[7]);


  flg = synchronize(txCmd,sizeof txCmd,rxCmd,sizeof rxCmd);
  if(flg == false)
  {
   return -1;
  }
  reData = rxCmd[2];
  return reData;
}
//RAM読み込み
/*
* @brief 
* @param id 
* @param Address 読み込み先頭アドレス
* @param Length 読み込みデータの長さ
*/
int b3mBaseClass::ReadCmd(byte id,byte Address,byte *rxCmd,byte rxLen){
  byte txCmd[7];
  bool flg;

  txCmd[0] = (byte)(0x07);
  txCmd[1] = (byte)(0x03);
  txCmd[2] = (byte)(0x00);
  txCmd[3] = (byte)(id);
  txCmd[4] = (byte)(Address);
  txCmd[5] = (byte)(rxLen-0x04);

  for(int i = 0 ; i<6; i++){
    txCmd[6] +=txCmd[i];
    //Serial.println(txCmd[i]);
  }
  txCmd[6] = (byte)(txCmd[6]);


  flg = synchronize(txCmd,sizeof txCmd,rxCmd,rxLen);
  if(flg == false)
  {
   return -1;
  }
  return 1;
}
//servo initialize
/*
*
*/
bool b3mBaseClass::b3m_init(byte id){

  WriteCmd(id,0x02,0x28);delay(100);//脱力
  WriteCmd(id,0x02,0x28);delay(100);//control-pos
  WriteCmd(id,0x00,0x29);delay(100);//軌道-Normal
  WriteCmd(id,0x00,0x5C);delay(100);//gainpreset-pos
  WriteCmd(id,0x00,0x28);delay(100);//位置制御開始

  return true;
}
//サーボ角度セット //////////////////////////////////////////////////////////////////////////////////////////
/**
* @brief サーボモータの角度を変更します
* @param[in] id サーボモータのID番号
* @param[in] pos ポジションデータ
* @return ステータス
* @retval -1 範囲外、通信失敗
**/
int b3mBaseClass::setPos(byte id,unsigned int Pos){
  byte txCmd[9];
  byte rxCmd[7];
  unsigned int reData;
  bool flg;

  txCmd[0] = (byte)(0x09);
  txCmd[1] = (byte)(0x06);
  txCmd[2] = (byte)(0x00);
  txCmd[3] = (byte)(id);
  
  txCmd[4] = (byte)(Pos & 0xFF);
  txCmd[5] = (byte)(Pos >> 8 & 0xFF);
  
  txCmd[6] = (byte)(0x00);
  txCmd[7] = (byte)(0x00);

  txCmd[8] = 0x00;

  for(int i = 0 ; i<8; i++){
    txCmd[8] +=txCmd[i];
  }
  txCmd[8] = (byte)(txCmd[8]);

  delay(10);
  flg = synchronize(txCmd,sizeof txCmd,rxCmd,sizeof rxCmd);
 
  if(flg == false)
  {
   return -1;
  }
  reData = rxCmd[2];
  return reData;
}
int b3mBaseClass::setPos(byte id, unsigned int Pos,unsigned int Time){
  byte txCmd[9];
  byte rxCmd[7];
  unsigned int reData;
  bool flg;

  txCmd[0] = (byte)(0x09);
  txCmd[1] = (byte)(0x06);
  txCmd[2] = (byte)(0x00);
  txCmd[3] = (byte)(id);
  
  txCmd[4] = (byte)(Pos & 0xFF);
  txCmd[5] = (byte)(Pos >> 8 & 0xFF);
  
  txCmd[6] = (byte)(Time & 0xFF);
  txCmd[7] = (byte)(Time >> 8 & 0xFF);

  txCmd[8] = 0x00;

  for(int i = 0 ; i<8; i++){
    txCmd[8] +=txCmd[i];
  }
  txCmd[8] = (byte)(txCmd[8]);

  delay(10);
  flg = synchronize(txCmd,sizeof txCmd,rxCmd,sizeof rxCmd);
  if(flg == false)
  {
   return -1;
  }
  reData = rxCmd[2];
  return reData;

}
//サーボ脱力 //////////////////////////////////////////////////////////////////////////////////////////
/**
* @brief サーボモータを脱力、setNomal関数によりトルク復活
* @param[in] id サーボモータのID番号
* @return ステータス
* @retval -1 範囲外、通信失敗
**/
bool b3mBaseClass::setFree(byte id){
  delay(10);
  return WriteCmd(id,0x02,0x28);delay(100);//脱力
}
//サーボトルク //////////////////////////////////////////////////////////////////////////////////////////
/**
* @brief トルク復活
* @param[in] id サーボモータのID番号
* @return ステータス
* @retval -1 範囲外、通信失敗
**/
bool b3mBaseClass::setNormal(byte id){
  delay(10);
  return WriteCmd(id,0x00,0x28);delay(100);//駆動
}
//サーボ角度入手 //////////////////////////////////////////////////////////////////////////////////////////
/**
* @brief サーボモータの角度入手
* @param[in] id サーボモータのID番号
* @return ポジション
* @retval -1 範囲外、通信失敗
**/
int b3mBaseClass::getPos(byte id){
  int pos;
  byte rxD[6];
  delay(10);
  ReadCmd(id,0x2C,rxD,sizeof rxD);//pos取得
  
  pos = rxD[4]+(rxD[5]<<8); //ポジションに変換
  if(pos>32767){            //負の数処理
        pos = pos-65535;
    }

  return pos;
}
//id入手(単一接続の場合のみ) //////////////////////////////////////////////////////////////////////////////////////////
/**
* @brief サーボモータのid入手
* @return id
* @retval -1 範囲外、通信失敗
**/
 int b3mBaseClass::getID(){
  int id;
  byte rxD[5];

  delay(10);//これが必要
  ReadCmd(0xff,0x00,rxD,sizeof rxD);

  return rxD[5];
 }