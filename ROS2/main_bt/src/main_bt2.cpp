#include "behaviortree_cpp/bt_factory.h"
#include "../include/my_action_node2.hpp"
#include "ament_index_cpp/get_package_share_directory.hpp"
#include "rclcpp/rclcpp.hpp"
#include "../include/my_ros_node2.hpp"

using namespace MyActionNodes;
using namespace BT;


int main(int argc, char* argv[]){
  rclcpp::init(argc, argv);
  global_node = std::make_shared<BtRosNode>();
  BehaviorTreeFactory factory;
  factory.registerNodeType<RobotOK>("RobotOK");
  factory.registerNodeType<BoxCheck>("BoxCheck");
  factory.registerNodeType<UpdateBB>("UpdateBB");
  factory.registerNodeType<UpArm>("UpArm");
  factory.registerNodeType<DownArm>("DownArm");
  factory.registerNodeType<MoveArm>("MoveArm");
  factory.registerNodeType<HandChange>("HandChange");
  factory.registerNodeType<Search>("Search");
  factory.registerNodeType<SpeakPub>("SpeakPub");
  factory.registerNodeType<RememberArm>("RememberArm");
  factory.registerNodeType<PubGripSuccess>("PubGripSuccess");
  factory.registerNodeType<PubCollectSuccess>("PubCollectSuccess");
  factory.registerNodeType<PubAutoFin>("PubAutoFin");
  factory.registerNodeType<OpenHand>("OpenHand");
  factory.registerNodeType<CloseHand>("CloseHand");
  factory.registerNodeType<PortInit>("PortInit");
  std::string package_path = ament_index_cpp::get_package_share_directory("main_bt");
  factory.registerBehaviorTreeFromFile(package_path + "/config/MainBT2.xml");
  BT::Tree tree = factory.createTree("MainTree");
  printTreeRecursively(tree.rootNode());
  NodeStatus status = NodeStatus::RUNNING;

  while(status == NodeStatus::RUNNING && rclcpp::ok()){
    rclcpp::spin_some(global_node);
    status = tree.tickOnce();
  }

  rclcpp::shutdown();
  return 0;
}



