/*
自動制御と手動制御が混ざったモード。
*/
#pragma once
#include <memory>
#include <string>
#include <rclcpp/allocator/allocator_common.hpp>
#include "behaviortree_cpp/behavior_tree.h"
#include "behaviortree_cpp/bt_factory.h"
#include "my_ros_node2.hpp"
#include <rclcpp/executors.hpp>
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include "algorithm"

using namespace BT;

Point left_box_pos_empty = {0.0, 20.0}; //[mm]
Point left_box_pos_fill = {20.0, 20.0};
Point right_box_pos_empty = {180.0, 20.0};
Point right_box_pos_fill = {200.0, 20.0};
Point common_pos = {677.0, 0.0};
std::vector<Point> left_own_pos = {{227.5, -400.0}, {227.5, -200.0}, {227.5, 0.0}, {227.5, 200.0}, {227.5, 400.0}};
std::vector<Point> right_own_pos = {{227.5, 400.0}, {227.5, 200.0}, {227.5, 0.0}, {227.5, -200.0}, {227.5, -400.0}};
//double point_distance = 3; //[mm]
//int min_cluster_size = 10;

namespace BT{
    template <> inline Point convertFromString(StringView str)
    {
        // We expect real numbers separated by semicolons
        auto parts = splitString(str, ';');
        if (parts.size() != 2)
        {
            throw RuntimeError("invalid input)");
        }
        else
        {
            Point output;
            output.x     = convertFromString<double>(parts[0]);
            output.y     = convertFromString<double>(parts[1]);
            return output;
        }
    }
}

namespace MyActionNodes{

//"RobotOK"
    class RobotOK : public StatefulActionNode
    {
    public:
        RobotOK(const std::string& name) : StatefulActionNode(name, {}){ }

        NodeStatus onStart() override
        {
            std::cout << "call RobotOK" << std::endl;
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool robot_start = false;
            global_node->sub_robot_start(robot_start);
            if(robot_start){
                std::cout << "main bt start!" << std::endl;
                return NodeStatus::SUCCESS;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return NodeStatus::RUNNING;
        }

        void onHalted() override{
            std::cout << "interrupt RobotOK Node" << std::endl;
        }
    };

//"BoxCheck" StatefulActionNode
//input_port "field_state"
//output_port "box_pos"
    class BoxCheck : public StatefulActionNode
    {
    public:
        BoxCheck(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<Point>("box_pos"),
                     InputPort<int>("field_state") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call BoxCheck" << std::endl;
            global_node->sub_box_on(box_on);
            if(box_on){
                std::cout << "Box_on OK!" << std::endl;
                Expected<int> msg = getInput<int>("field_state");
                if (!msg)
                {
                    throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
                }
                int field_state = msg.value();
                Point box_pos;
                if(field_state == 1){
                    if(box_fill){
                        setOutput("box_pos", right_box_pos_fill);
                    }else{
                        setOutput("box_pos", right_box_pos_empty);
                    }
                }else if(field_state == 2){
                    if(box_fill){
                        setOutput("box_pos", left_box_pos_fill);
                    }else{
                        setOutput("box_pos", left_box_pos_empty);
                    }
                }else{
                    std::cout << "field state error at BoxCheck" << std::endl;
                    return NodeStatus::FAILURE;
                }
                return NodeStatus::SUCCESS;
            }
            std::cout << "wait for box_on" << std::endl;
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            global_node->sub_box_on(box_on);
            if(box_on){
                std::cout << "Box_on OK!" << std::endl;
                Expected<int> msg = getInput<int>("field_state");
                if (!msg)
                {
                    throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
                }
                int field_state = msg.value();
                Point box_pos;
                if(field_state == 1){
                    if(box_fill){
                        setOutput("box_pos", right_box_pos_fill);
                    }else{
                        setOutput("box_pos", right_box_pos_empty);
                    }
                }else if(field_state == 2){
                    if(box_fill){
                        setOutput("box_pos", left_box_pos_fill);
                    }else{
                        setOutput("box_pos", left_box_pos_empty);
                    }
                }else{
                    std::cout << "field state error at BoxCheck" << std::endl;
                    return NodeStatus::FAILURE;
                }
                return NodeStatus::SUCCESS;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return NodeStatus::RUNNING;
        }

        void onHalted() override{
            std::cout << "interrupt BoxCheck Node" << std::endl;
        }

    private:
        bool box_fill = false;
        bool box_on = false;
    };

//"UpdateBB" StatefulActionNode
//output_port "field_pos"
//output_port "linear_state"
    class UpdateBB : public StatefulActionNode
    {
    public:
        UpdateBB(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<int>("field_pos"),
                     OutputPort<std::string>("linear_state") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call UpdateBB" << std::endl;
            global_node->sub_field_pos(field_state);
            setOutput("field_pos", (int)field_state);
            global_node->sub_linear_auto(linear_auto);
            global_node->sub_hand_grip(hand_grip);
            global_node->sub_hand_collect(hand_collect);
            if(hand_grip && !hand_collect && !linear_auto){
                linear_state = "1";
            }else if(!hand_grip && hand_collect && !linear_auto){
                linear_state = "2";
            }else if(!hand_grip && !hand_collect && linear_auto){
                linear_state = "3";
            }else{
                linear_state = "0";
            }
            setOutput("linear_state", linear_state);
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            std::cout << "------" << std::endl;
            global_node->sub_field_pos(field_state);
            setOutput("field_pos", (int)field_state);
            global_node->sub_linear_auto(linear_auto);
            global_node->sub_hand_grip(hand_grip);
            global_node->sub_hand_collect(hand_collect);
            if(hand_grip && !hand_collect && !linear_auto){
                linear_state = "1";
            }else if(!hand_grip && hand_collect && !linear_auto){
                linear_state = "2";
            }else if(!hand_grip && !hand_collect && linear_auto){
                linear_state = "3";
            }else{
                linear_state = "0";
            }
            setOutput("linear_state", linear_state);
            return NodeStatus::RUNNING;
        }

        void onHalted() override{
            std::cout << "interrupt UpdateBB Node" << std::endl;
        }
    private:
        short field_state = 0;
        std::string linear_state = "0";
        bool linear_auto = false;
        bool hand_grip = false;
        bool hand_collect = false;
    };

//"UpArm" StatefulActionNode
//input_port "up_length"
    class UpArm : public StatefulActionNode
    {
    public:
        UpArm(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<double>("up_length") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call UpArm" << std::endl;
            Expected<double> msg = getInput<double>("up_length");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [up_length]: ", msg.error() );
            }
            up_length = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_hand_z((float)up_length);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt UpArm Node" << std::endl;
        }
    private:
        double up_length = 0.0;
    };


//"DownArm" StatefulActionNode
//input_port "down_length"
    class DownArm : public StatefulActionNode
    {
    public:
        DownArm(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<double>("down_length") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call DownArm" << std::endl;
            Expected<double> msg = getInput<double>("down_length");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [down_length]: ", msg.error() );
            }
            down_length = -msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_hand_z((float)down_length);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt DownArm Node" << std::endl;
        }
    private:
        double down_length = 0.0;
    };

//"MoveArm" StatefulActionNode
//input_port "move_pos"
    class MoveArm : public StatefulActionNode
    {
    public:
        MoveArm(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<Point>("move_pos") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call MoveArm" << std::endl;
            Expected<Point> msg = getInput<Point>("move_pos");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [move_pos]: ", msg.error() );
            }
            move_pos = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_linear_pos((float)move_pos.x, (float)move_pos.y);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt MoveArm Node" << std::endl;
        }
    private:
        Point move_pos = {0.0, 0.0};
    };

//"HandChange" StatefulActionNode
//input_port "hand_mode"
//input_port "turn_deg"
    class HandChange : public StatefulActionNode
    {
    public:
        HandChange(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<int>("hand_mode"),
                     InputPort<double>("turn_deg") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call HandChange" << std::endl;
            Expected<double> turn = getInput<double>("turn_deg");
            if (!turn)
            {
                throw BT::RuntimeError("missing required input [turn_deg]: ", turn.error() );
            }
            turn_deg = turn.value();
            Expected<int> mode = getInput<int>("hand_mode");
            if (!mode)
            {
                throw BT::RuntimeError("missing required input [hand_mode]: ", mode.error() );
            }
            hand_mode = mode.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_hand_mode((float)turn_deg, hand_mode);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt HandChange Node" << std::endl;
        }
    private:
        int hand_mode = 0;
        double turn_deg = 0.0;
    };

//"Search" StatefulActionNode
//input_port "field_state"
//output_port "detect_pos"
    class Search : public StatefulActionNode
    {
    public:
        Search(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<Point>("detect_pos"),
                     InputPort<int>("field_state") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call Search" << std::endl;
            Expected<int> msg = getInput<int>("field_state");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
            }
            field_state = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            Point detect_pos = {0.0, 0.0};
            if(count == 5){
                return NodeStatus::FAILURE;
            }
            if(field_state == 1){
                detect_pos = right_own_pos[count];
                count++;
            }else if(field_state == 2){
                detect_pos = left_own_pos[count];
                count++;
            }
            setOutput("detect_pos", detect_pos);
            return NodeStatus::SUCCESS;
            /*
            std::vector<Point> points = {};
            global_node->sub_scan(points); //10の点群の組を平均化した１つの点群が得られる。
            int size = points.size();
            std::vector<std::vector<Point>> clusters;
            for(int i = 0; i < size ; i++){ //すべての点に対して操作を行う
                bool cluster_ok = false;
                for(int j = 0; j < clusters.size(); j++){ //すべてのクラスターに対して比較を行う。
                    for(int k = 0; k < clusters[j].size(); k++){ //クラスターのすべての点に対して比較を行う。
                        if(calculateDistance(clusters[j][k], points[i]) < point_distance){ //もしクラスター内に近い点があったらそのクラスターに点を加える
                            clusters[j].push_back(points[i]);
                            cluster_ok = true;
                            break;
                        }
                    }
                    if(cluster_ok){
                        break;
                    }
                }
                if(!cluster_ok){
                    std::vector<Point> one_point = {points[i]};
                    clusters.push_back(one_point); //もしすべてのクラスターに対して近い点がなければ、新たなクラスターを作成する
                }
            }
            size = clusters.size();
                //小さいクラスターは切り捨てた上で、点群の平均を取る。
            std::vector<Point> resultPoints;
            for (int i = 0; i < size; i++) {
                int cluster_size = clusters[i].size();
                if (cluster_size >= min_cluster_size) {
                    double totalX = 0.0;
                    double totalY = 0.0;
                    for (const Point& p : clusters[i]) {
                        totalX += p.x;
                        totalY += p.y;
                    }
                    double averageX = totalX / cluster_size;
                    double averageY = totalY / cluster_size;
                    resultPoints.push_back({averageX, averageY});
                }
            }
            size = resultPoints.size();
            std::cout << "limit clustered point size:" << size << std::endl;
            if (size == 0) {
                return NodeStatus::FAILURE;
            }

            Expected<int> msg = getInput<int>("field_state");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
            }
            int field_state = msg.value();
            int idx = 0;
            if(field_state == 1){
                double num = -5000.0;
                for(int i = 0; i < size; i++){
                    if(resultPoints[i].x > num){
                        num = resultPoints[i].x;
                        idx = i;
                    }
                }
            }else if(field_state == 2){
                double num = 5000.0;
                for(int i = 0; i < size; i++){
                    if(resultPoints[i].x < num){
                        num = resultPoints[i].x;
                        idx = i;
                    }
                }
            }else{
                std::cout << "field_state error at Search" << std::endl;
                return NodeStatus::FAILURE;
            }
            Point detect_pos = resultPoints[idx];
            setOutput("detect_pos", detect_pos);
            return NodeStatus::SUCCESS;
            */
        }

        void onHalted() override{
            std::cout << "interrupt Search Node" << std::endl;
        }
    private:
        int field_state = 0;
        int count = 0;
    /*
        double calculateDistance(const Point& p1, const Point& p2) {
            return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));
        }
        */
    };

//"SpeakPub" SyncActionNode
//input_port "speak_num"
    class SpeakPub : public SyncActionNode
    {
    public:
        SpeakPub(const std::string& name, const NodeConfig& config) : SyncActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<int>("speak_num") };
        }

        NodeStatus tick() override
        {
            int speak_num = 0;
            std::cout << "call SpeakPub" << std::endl;
            Expected<int> msg = getInput<int>("speak_num");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [speak_num]: ", msg.error() );
            }
            speak_num = msg.value();
            global_node->pub_speak((short)speak_num);
            return NodeStatus::SUCCESS;
        }
    };

//"RememberArm" SyncActionNode
//output_port "remember_pos"
    class RememberArm : public SyncActionNode
    {
    public:
        RememberArm(const std::string& name, const NodeConfig& config) : SyncActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<Point>("remember_pos") };
        }

        NodeStatus tick() override
        {
            std::cout << "call RememberArm" << std::endl;
            //geometry_msgs::msg::Twist arm_pos;
            //global_node->sub_linear_state(arm_pos);
            setOutput("remember_pos", common_pos);
            return NodeStatus::SUCCESS;
        }
    };

//"PubGripSuccess" SyncActionNode
    class PubGripSuccess : public SyncActionNode
    {
    public:
        PubGripSuccess(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call PubGripSuccess" << std::endl;
            global_node->pub_grip_comp(true);
            return NodeStatus::SUCCESS;
        }
    };

//"PubCollectSuccess" SyncActionNode
    class PubCollectSuccess : public SyncActionNode
    {
    public:
        PubCollectSuccess(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call PubCollectSuccess" << std::endl;
            global_node->pub_collect_comp(true);
            return NodeStatus::SUCCESS;
        }
    };

//"PubAutoFin" SyncActionNode
    class PubAutoFin : public SyncActionNode
    {
    public:
        PubAutoFin(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call PubAutoFin" << std::endl;
            global_node->pub_linear_comp(true);
            return NodeStatus::SUCCESS;
        }
    };

//"OpenHand" SyncActionNode
    class OpenHand : public SyncActionNode
    {
    public:
        OpenHand(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call OpenHand" << std::endl;
            global_node->pub_air_on(true);
            return NodeStatus::SUCCESS;
        }
    };

//"CloseHand" SyncActionNode
    class CloseHand : public SyncActionNode
    {
    public:
        CloseHand(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call CloseHand" << std::endl;
            global_node->pub_air_on(false);
            return NodeStatus::SUCCESS;
        }
    };

//"PortInit"
//output_port "init_port"
    class PortInit : public SyncActionNode
    {
    public:
        PortInit(const std::string& name, const NodeConfig& config) : SyncActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<double>("init_port") };
        }

        NodeStatus tick() override
        {
            std::cout << "call PortInit" << std::endl;
            double init_port = 100.0;
            setOutput("init_port", init_port);
            return NodeStatus::SUCCESS;
        }
    };
}
