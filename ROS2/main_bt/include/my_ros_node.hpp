#pragma once
#include <chrono> //計測用，xxmsという書き方ができるようにする
#include <functional> //ラムダ式などを使えるようにする
#include <memory> //メモリを扱えるようにする
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/bool.hpp"
#include "std_msgs/msg/int16.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include "catchrobo_msgs/srv/linear.hpp"
#include "catchrobo_msgs/srv/hand.hpp"

using namespace std::chrono_literals;

struct Point {
    double x;
    double y;
};
/*
double Deg2Rad(double deg){
    return deg/180.0 * M_PI;
}

const double angle_min = Deg2Rad(-80.0);
const double angle_max = Deg2Rad(80.0);
const double x_limit[] = {-600.0, 600.0}; //[mm]
const double y_limit[] = {0.0, 175.0}; //[mm]
const int scan_pool_size = 10;
*/
class BtRosNode : public rclcpp::Node{ //rclcpp::Nodeを継承してMinimalPubSubクラスを作成
    public:

    BtRosNode() : Node("bt_ros_node"){ //Node関数をオーバーライド
        std::cout << "bt_ros_node is called" << std::endl;
        
        auto field_pos_cb = [this](const std_msgs::msg::Int16& msg) -> void{field_pos_data = msg.data;};
        auto linear_auto_cb = [this](const std_msgs::msg::Bool& msg) -> void{linear_auto_data = msg.data;};
        auto robot_start_cb = [this](const std_msgs::msg::Bool& msg) -> void{robot_start_data = msg.data;};
//        auto hand_grip_cb = [this](const std_msgs::msg::Bool& msg) -> void{hand_grip_data = msg.data;};
//        auto hand_collect_cb = [this](const std_msgs::msg::Bool& msg) -> void{hand_collect_data = msg.data;};
        auto box_on_cb = [this](const std_msgs::msg::Bool& msg) -> void{box_on_data = msg.data;};
        auto interrupt_on_cb = [this](const std_msgs::msg::Bool& msg) -> void{interrput_on_data = msg.data;};
        //auto linear_state_cb = [this](const geometry_msgs::msg::Twist& msg) -> void{linear_state_data = msg;};
//        auto scan_cb = [this](const sensor_msgs::msg::LaserScan& msg) -> void{
//            scan_data.push_back(msg);
//            if(scan_data.size() > scan_pool_size){
//                scan_data.erase(scan_data.begin());
//            }
//        };

        rclcpp::QoS qos(rclcpp::KeepLast(10));

        linear_comp_pub = this->create_publisher<std_msgs::msg::Bool>("/linear_comp", qos);
//        grip_comp_pub = this->create_publisher<std_msgs::msg::Bool>("/grip_comp", qos);
//        collect_comp_pub = this->create_publisher<std_msgs::msg::Bool>("/collect_comp", qos);
        air_on_pub = this->create_publisher<std_msgs::msg::Bool>("/air_on", qos);
        speak_pub = this->create_publisher<std_msgs::msg::Int16>("/speak", qos);
        field_pos_sub = this->create_subscription<std_msgs::msg::Int16>("/field_pos", qos, field_pos_cb);
        linear_auto_sub = this->create_subscription<std_msgs::msg::Bool>("/linear_auto", qos, linear_auto_cb);
        robot_start_sub = this->create_subscription<std_msgs::msg::Bool>("/robot_start", qos, robot_start_cb);
//        hand_grip_sub = this->create_subscription<std_msgs::msg::Bool>("/hand_grip", qos, hand_grip_cb);
//        hand_collect_sub = this->create_subscription<std_msgs::msg::Bool>("/hand_collect", qos, hand_collect_cb);
        box_on_sub = this->create_subscription<std_msgs::msg::Bool>("/box_on", qos, box_on_cb);
        interrput_on_sub = this->create_subscription<std_msgs::msg::Bool>("/interrput_on", qos, interrupt_on_cb);
        //linear_state_sub = this->create_subscription<geometry_msgs::msg::Twist>("/linear_state", qos, linear_state_cb);
//        scan_sub = this->create_subscription<sensor_msgs::msg::LaserScan>("/scan", qos, scan_cb);

        linear_cli = create_client<catchrobo_msgs::srv::Linear>("/linear_service");
        while(!linear_cli->wait_for_service(1s)){
            if(!rclcpp::ok()){
                break;
            }
            std::cout << "linear service not available" << std::endl;
        }
        hand_cli = create_client<catchrobo_msgs::srv::Hand>("/hand_service");
        while(!hand_cli->wait_for_service(1s)){
            if(!rclcpp::ok()){
                break;
            }
            std::cout << "hand service not available" << std::endl;
        }
    }
    
    void pub_linear_comp(bool msg){
        auto pub_msg = std_msgs::msg::Bool();
        pub_msg.data = msg;
        this->linear_comp_pub->publish(pub_msg);
    }
    /*
    void pub_grip_comp(bool msg){
        auto pub_msg = std_msgs::msg::Bool();
        pub_msg.data = msg;
        this->grip_comp_pub->publish(pub_msg);
    }
    void pub_collect_comp(bool msg){
        auto pub_msg = std_msgs::msg::Bool();
        pub_msg.data = msg;
        this->collect_comp_pub->publish(pub_msg);
    }
    */
    void pub_air_on(bool msg){
        auto pub_msg = std_msgs::msg::Bool();
        pub_msg.data = msg;
        this->air_on_pub->publish(pub_msg);
    }
    void pub_speak(short msg){
        auto pub_msg = std_msgs::msg::Int16();
        pub_msg.data = msg;
        this->speak_pub->publish(pub_msg);
    }
    void sub_field_pos(short& msg){
        msg = this->field_pos_data;
    }
    void sub_linear_auto(bool& msg){
        msg = this->linear_auto_data;
    }
    void sub_robot_start(bool& msg){
        msg = this->robot_start_data;
    }
//    void sub_hand_grip(bool& msg){
//        msg = this->hand_grip_data;
//    }
//    void sub_hand_collect(bool& msg){
//        msg = this->hand_collect_data;
//    }
    void sub_box_on(bool& msg){
        msg = this->box_on_data;
    }
    void sub_interrput_on(bool& msg){
        msg = this->interrput_on_data;
    }
 //   void sub_linear_state(geometry_msgs::msg::Twist& msg){
 //       msg = this->linear_state_data;
 //   }
 /*
    void sub_scan(std::vector<Point>& points){
        int size = this->scan_data.size();
        int point_num = this->scan_data[0].ranges.size();
        double angle_increment = this->scan_data[0].angle_increment;
        int under = static_cast<int>((angle_min - this->scan_data[0].angle_min)/angle_increment);
        int over = static_cast<int>((this->scan_data[0].angle_max - angle_max)/angle_increment);
        std::vector<float> limit_ranges(point_num-under-over, 0.0f);
        // 指定角度をオーバーしている点群を捨てる。距離の和を取る。
        for(int i = 0; i < size; i++){
            int k = 0;
            for(int j = under + 1; j < point_num - over; j++){
                limit_ranges[k] += this->scan_data[i].ranges[j];
                k++;
            }
        }
        
        int limit_ranges_size = limit_ranges.size();
        double angle = angle_min + M_PI/2.0;
        //座標をデカルト座標に変換。平均データを求め、制限よりオーバーした点群を捨てる。
        for(int i = 0; i < limit_ranges_size; i++){
            double range = limit_ranges[i] / size;
            double x_point = range * std::cos(angle) * 1000.0; //メートルからミリメートルに単位変更
            double y_point = range * std::sin(angle) * 1000.0 - 30.0; //LiDARの位置の分を補正
            angle += angle_increment;
            if (x_limit[0] < x_point && x_point < x_limit[1] && y_limit[0] < y_point && y_point < y_limit[1]){
                Point new_point = {x_point, y_point};
                points.push_back(new_point);
            }
        }
    }*/

    bool send_linear_pos(float x, float y){
        auto req = std::make_shared<catchrobo_msgs::srv::Linear::Request>();
        bool result = false;
        req->x0 = x*0.462917;
        req->x1 = x*0.537082;
        req->y = y;
        auto res_received_cb = [&result](rclcpp::Client<catchrobo_msgs::srv::Linear>::SharedFuture future){
            auto res = future.get();
            result = res->complete;
        };
        auto future_result = linear_cli->async_send_request(req, res_received_cb);
        return result;
    }
    // 修正すること

    bool send_hand_z(float z){
        auto req = std::make_shared<catchrobo_msgs::srv::Hand::Request>();
        bool result = false;
        req->z = z; //own:130, common:83.67, collect:130
        req->turn = 500.0;
        req->mode = 500.0;
        auto res_received_cb = [&result](rclcpp::Client<catchrobo_msgs::srv::Hand>::SharedFuture future){
            auto res = future.get();
            result = res->complete;
        };
        auto future_result = hand_cli->async_send_request(req, res_received_cb);
        return result;
    }
    bool send_hand_mode(float turn, int mode){
        auto req = std::make_shared<catchrobo_msgs::srv::Hand::Request>();
        bool result = false;
        req->z = 500;
        req->turn = turn; // own:0, common:-90, collect:-90
        if(mode == 1){ //own
            req->mode = -130.0;
        }else if(mode == 2){ //common
            req->mode = -7.0;
        }else if(mode == 3){ //collect
            req->mode = 90.0;
        }else{
            std::cout << "send_hand_mode: input mode error" << std::endl;
        }
        auto res_received_cb = [&result](rclcpp::Client<catchrobo_msgs::srv::Hand>::SharedFuture future){
            auto res = future.get();
            result = res->complete;
        };
        auto future_result = hand_cli->async_send_request(req, res_received_cb);
        return result;
    }

    private:
    short field_pos_data = 0;
    bool linear_auto_data = false;
    bool robot_start_data = false;
//    bool hand_grip_data = false;
//    bool hand_collect_data = false;
    bool box_on_data = false;
    bool interrput_on_data = false;
    //geometry_msgs::msg::Twist linear_state_data;
//    std::vector<sensor_msgs::msg::LaserScan> scan_data;
    
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr linear_comp_pub;
//    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr grip_comp_pub;
//    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr collect_comp_pub;
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr air_on_pub;
    rclcpp::Publisher<std_msgs::msg::Int16>::SharedPtr speak_pub;
    rclcpp::Subscription<std_msgs::msg::Int16>::SharedPtr field_pos_sub;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr linear_auto_sub;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr robot_start_sub;
//    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr hand_grip_sub;
//    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr hand_collect_sub;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr box_on_sub;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr interrput_on_sub;
    //rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr linear_state_sub;
//    rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr scan_sub;
    rclcpp::Client<catchrobo_msgs::srv::Linear>::SharedPtr linear_cli;
    rclcpp::Client<catchrobo_msgs::srv::Hand>::SharedPtr hand_cli;
};

std::shared_ptr<BtRosNode> global_node;
