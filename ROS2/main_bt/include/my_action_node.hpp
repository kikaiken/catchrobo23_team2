/*
すべてのワークを自動制御で取るモード
スマホの自動ボタンで自動モード開始、妨害機構ボタンをオンにすると共通エリア、オフにすると自陣エリアのワークを取得する。
*/


#pragma once
#include <memory>
#include <string>
#include <rclcpp/allocator/allocator_common.hpp>
#include "behaviortree_cpp/behavior_tree.h"
#include "behaviortree_cpp/bt_factory.h"
#include "my_ros_node.hpp"
#include <rclcpp/executors.hpp>
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include "algorithm"

using namespace BT;

Point left_box_pos_empty = {0.0, 20.0}; //[mm]
Point left_box_pos_fill = {20.0, 20.0};
Point right_box_pos_empty = {180.0, 20.0}; //適当
Point right_box_pos_fill = {200.0, 20.0};
std::vector<Point> left_common_pos = {{677.0, -420.0}, {677.0, 0.0}, {677.0, 420.0}};
std::vector<Point> right_common_pos = {{677.0, 420.0}, {677.0, 0.0}, {677.0, -420.0}}; //　ハンドの変形を勘案していないので補正すること。
std::vector<Point> left_own_pos = {{227.5, -400.0}, {227.5, -200.0}, {227.5, 0.0}, {227.5, 200.0}, {227.5, 400.0}};
std::vector<Point> right_own_pos = {{227.5, 400.0}, {227.5, 200.0}, {227.5, 0.0}, {227.5, -200.0}, {227.5, -400.0}};
//double point_distance = 3; //[mm]
//int min_cluster_size = 10;

namespace BT{
    template <> inline Point convertFromString(StringView str)
    {
        // We expect real numbers separated by semicolons
        auto parts = splitString(str, ';');
        if (parts.size() != 2)
        {
            throw RuntimeError("invalid input)");
        }
        else
        {
            Point output;
            output.x     = convertFromString<double>(parts[0]);
            output.y     = convertFromString<double>(parts[1]);
            return output;
        }
    }
}

namespace MyActionNodes{

//"RobotOK"
    class RobotOK : public StatefulActionNode
    {
    public:
        RobotOK(const std::string& name) : StatefulActionNode(name, {}){ }

        NodeStatus onStart() override
        {
            std::cout << "call RobotOK" << std::endl;
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool robot_start = false;
            global_node->sub_robot_start(robot_start);
            global_node->sub_linear_auto(linear_auto);
            if(robot_start && linear_auto){
                std::cout << "main bt start!" << std::endl;
                return NodeStatus::SUCCESS;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return NodeStatus::RUNNING;
        }

        void onHalted() override{
            std::cout << "interrupt RobotOK Node" << std::endl;
        }
    private:
        bool linear_auto = false;
    };

//"BoxCheck" StatefulActionNode
//input_port "field_state"
//output_port "box_pos"
    class BoxCheck : public StatefulActionNode
    {
    public:
        BoxCheck(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<Point>("box_pos"),
                     InputPort<int>("field_state") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call BoxCheck" << std::endl;
            global_node->sub_box_on(box_on);
            if(box_on){
                std::cout << "Box_on OK!" << std::endl;
                Expected<int> msg = getInput<int>("field_state");
                if (!msg)
                {
                    throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
                }
                int field_state = msg.value();
                Point box_pos;
                if(field_state == 1){
                    if(box_fill){
                        setOutput("box_pos", right_box_pos_fill);
                    }else{
                        setOutput("box_pos", right_box_pos_empty);
                    }
                }else if(field_state == 2){
                    if(box_fill){
                        setOutput("box_pos", left_box_pos_fill);
                    }else{
                        setOutput("box_pos", left_box_pos_empty);
                    }
                }else{
                    std::cout << "field state error at BoxCheck" << std::endl;
                    return NodeStatus::FAILURE;
                }
                return NodeStatus::SUCCESS;
            }
            std::cout << "wait for box_on" << std::endl;
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            global_node->sub_box_on(box_on);
            if(box_on){
                std::cout << "Box_on OK!" << std::endl;
                Expected<int> msg = getInput<int>("field_state");
                if (!msg)
                {
                    throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
                }
                int field_state = msg.value();
                Point box_pos;
                if(field_state == 1){
                    if(box_fill){
                        setOutput("box_pos", right_box_pos_fill);
                    }else{
                        setOutput("box_pos", right_box_pos_empty);
                    }
                }else if(field_state == 2){
                    if(box_fill){
                        setOutput("box_pos", left_box_pos_fill);
                    }else{
                        setOutput("box_pos", left_box_pos_empty);
                    }
                }else{
                    std::cout << "field state error at BoxCheck" << std::endl;
                    return NodeStatus::FAILURE;
                }
                return NodeStatus::SUCCESS;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            return NodeStatus::RUNNING;
        }

        void onHalted() override{
            std::cout << "interrupt BoxCheck Node" << std::endl;
        }

    private:
        bool box_fill = false;
        bool box_on = false;
    };

//"UpdateBB" StatefulActionNode
//output_port "field_pos"
//output_port "linear_state"
    class UpdateBB : public StatefulActionNode
    {
    public:
        UpdateBB(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<int>("field_pos") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call UpdateBB" << std::endl;
            global_node->sub_field_pos(field_state);
            setOutput("field_pos", (int)field_state);
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            std::cout << "------" << std::endl;
            global_node->sub_field_pos(field_state);
            setOutput("field_pos", (int)field_state);
            return NodeStatus::RUNNING;
        }

        void onHalted() override{
            std::cout << "interrupt UpdateBB Node" << std::endl;
        }
    private:
        short field_state = 0;
    };

//"UpArm" StatefulActionNode
//input_port "up_length"
    class UpArm : public StatefulActionNode
    {
    public:
        UpArm(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<double>("up_length") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call UpArm" << std::endl;
            Expected<double> msg = getInput<double>("up_length");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [up_length]: ", msg.error() );
            }
            up_length = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_hand_z((float)up_length);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt UpArm Node" << std::endl;
        }
    private:
        double up_length = 0.0;
    };


//"DownArm" StatefulActionNode
//input_port "down_length"
    class DownArm : public StatefulActionNode
    {
    public:
        DownArm(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<double>("down_length") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call DownArm" << std::endl;
            Expected<double> msg = getInput<double>("down_length");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [down_length]: ", msg.error() );
            }
            down_length = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_hand_z((float)down_length);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt DownArm Node" << std::endl;
        }
    private:
        double down_length = 0.0;
    };

//"MoveArm" StatefulActionNode
//input_port "move_pos"
    class MoveArm : public StatefulActionNode
    {
    public:
        MoveArm(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<Point>("move_pos") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call MoveArm" << std::endl;
            Expected<Point> msg = getInput<Point>("move_pos");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [move_pos]: ", msg.error() );
            }
            move_pos = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_linear_pos((float)move_pos.x, (float)move_pos.y);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt MoveArm Node" << std::endl;
        }
    private:
        Point move_pos = {0.0, 0.0};
    };

//"HandChange" StatefulActionNode
//input_port "hand_mode"
//input_port "turn_deg"
    class HandChange : public StatefulActionNode
    {
    public:
        HandChange(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<int>("hand_mode"),
                     InputPort<double>("turn_deg") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call HandChange" << std::endl;
            Expected<double> turn = getInput<double>("turn_deg");
            if (!turn)
            {
                throw BT::RuntimeError("missing required input [turn_deg]: ", turn.error() );
            }
            turn_deg = turn.value();
            Expected<int> mode = getInput<int>("hand_mode");
            if (!mode)
            {
                throw BT::RuntimeError("missing required input [hand_mode]: ", mode.error() );
            }
            hand_mode = mode.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            bool send = global_node->send_hand_mode((float)turn_deg, hand_mode);
            if(send){
                return NodeStatus::SUCCESS;
            }else{
                return NodeStatus::FAILURE;
            }
        }

        void onHalted() override{
            std::cout << "interrupt HandChange Node" << std::endl;
        }
    private:
        int hand_mode = 0;
        double turn_deg = 0.0;
    };

//"Search" StatefulActionNode
//input_port "field_state"
//output_port "detect_pos"
    class Search : public StatefulActionNode
    {
    public:
        Search(const std::string& name, const NodeConfig& config) : StatefulActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<Point>("detect_pos"),
                     InputPort<int>("field_state"),
                     OutputPort<std::string>("work_area") };
        }

        NodeStatus onStart() override
        {
            std::cout << "call Search" << std::endl;
            Expected<int> msg = getInput<int>("field_state");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [field_state]: ", msg.error() );
            }
            field_state = msg.value();
            return NodeStatus::RUNNING;
        }

        NodeStatus onRunning() override{
            Point detect_pos = {0.0, 0.0};
            global_node->sub_interrput_on(own_erea);
            if(own_erea){
                if(own_count == 5){
                    if(common_count == 3){
                        return NodeStatus::FAILURE;
                    }
                    return NodeStatus::RUNNING;
                }
                if(field_state == 1){
                    detect_pos = right_own_pos[own_count];
                    own_count++;
                }else if(field_state == 2){
                    detect_pos = left_own_pos[own_count];
                    own_count++;
                }
            }else{
                if(common_count == 3){
                    if(own_count == 5){
                        return NodeStatus::FAILURE;
                    }
                    if(field_state == 1){
                        detect_pos = right_own_pos[own_count];
                        own_count++;
                    }else if(field_state == 2){
                        detect_pos = left_own_pos[own_count];
                        own_count++;
                    }
                }
                if(field_state == 1){
                    detect_pos = right_common_pos[common_count];
                    common_count++;
                }else if(field_state == 2){
                    detect_pos = left_common_pos[common_count];
                    common_count++;
                }
            }
            setOutput("detect_pos", detect_pos);
            if(own_erea){
                setOutput("work_area", "own");
            }else{
                setOutput("work_area", "common");
            }
            return NodeStatus::SUCCESS;
        }

        void onHalted() override{
            std::cout << "interrupt Search Node" << std::endl;
        }
    private:
        int field_state = 0;
        int common_count = 0;
        int own_count = 0;
        bool own_erea = false;
    };

//"SpeakPub" SyncActionNode
//input_port "speak_num"
    class SpeakPub : public SyncActionNode
    {
    public:
        SpeakPub(const std::string& name, const NodeConfig& config) : SyncActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { InputPort<int>("speak_num") };
        }

        NodeStatus tick() override
        {
            int speak_num = 0;
            std::cout << "call SpeakPub" << std::endl;
            Expected<int> msg = getInput<int>("speak_num");
            if (!msg)
            {
                throw BT::RuntimeError("missing required input [speak_num]: ", msg.error() );
            }
            speak_num = msg.value();
            global_node->pub_speak((short)speak_num);
            return NodeStatus::SUCCESS;
        }
    };

//"PubAutoFin" SyncActionNode
    class PubAutoFin : public SyncActionNode
    {
    public:
        PubAutoFin(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call PubAutoFin" << std::endl;
            global_node->pub_linear_comp(true);
            return NodeStatus::SUCCESS;
        }
    };

//"OpenHand" SyncActionNode
    class OpenHand : public SyncActionNode
    {
    public:
        OpenHand(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call OpenHand" << std::endl;
            global_node->pub_air_on(true);
            return NodeStatus::SUCCESS;
        }
    };

//"CloseHand" SyncActionNode
    class CloseHand : public SyncActionNode
    {
    public:
        CloseHand(const std::string& name) : SyncActionNode(name, {}){ }

        NodeStatus tick() override
        {
            std::cout << "call CloseHand" << std::endl;
            global_node->pub_air_on(false);
            return NodeStatus::SUCCESS;
        }
    };

//"PortInit"
//output_port "init_port"
    class PortInit : public SyncActionNode
    {
    public:
        PortInit(const std::string& name, const NodeConfig& config) : SyncActionNode(name, config){ }

        static PortsList providedPorts()
        {
            return { OutputPort<double>("init_port") };
        }

        NodeStatus tick() override
        {
            std::cout << "call PortInit" << std::endl;
            double init_port = 100.0;
            setOutput("init_port", init_port);
            return NodeStatus::SUCCESS;
        }
    };
}
