#include <micro_ros_arduino.h>
#include <SP_AudioPlayer.h>
//please install library https://github.com/baggio63446333/SP_Audio
#include <stdio.h>
#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>

#include <std_msgs/msg/int16.h>


static rcl_allocator_t allocator;
static rclc_support_t support;
static rcl_node_t node;
static rcl_subscription_t subscriber;
static rclc_executor_t executor;
rcl_init_options_t init_options;
rcl_node_options_t node_ops;
static std_msgs__msg__Int16 msg;
SP_AudioPlayer player;

#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){error_loop();}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){}}

void error_loop(){
  while(1){
    digitalWrite(LED0, !digitalRead(LED0));
    delay(100);
  }
}

void subscription_callback(const void * msgin) {  
  const std_msgs__msg__Int16 * msg = (const std_msgs__msg__Int16 *)msgin;
  if(-1 < msg->data && msg->data < 8){
    char filepath[10];
    sprintf(filepath, "%d%s", msg->data, ".wav");
    player.play(filepath, AS_CODECTYPE_WAV, AS_SAMPLINGRATE_48000, AS_BITLENGTH_16, AS_CHANNEL_STEREO);
    //player.wavplay(filepath);
  }
}

void setup() {
  set_microros_transports();
  
  delay(2000);
  
  allocator = rcl_get_default_allocator();
  init_options = rcl_get_zero_initialized_init_options();
  node_ops = rcl_node_get_default_options();
  RCCHECK(rcl_init_options_init(&init_options, allocator));
  RCCHECK(rcl_init_options_set_domain_id(&init_options, 123));
  RCCHECK(rclc_support_init_with_options(&support, 0, NULL, &init_options, &allocator));
  RCCHECK(rclc_node_init_with_options(&node, "spresense_node", "", &support, &node_ops));

  // create subscriber
  RCCHECK(rclc_subscription_init_default(
    &subscriber,
    &node,
    ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int16),
    "/speak"));

  // create executor
  RCCHECK(rclc_executor_init(&executor, &support.context, 1, &allocator));
  RCCHECK(rclc_executor_add_subscription(&executor, &subscriber, &msg, &subscription_callback, ON_NEW_DATA));

  player.begin();
  player.volume(10);
  player.setFs(48000); /* Change from the default 48kHz to 44.1kHz */
  player.setBitlen(16); /* same as the default */
  player.setChannel(2); /* same as the default */
  
  digitalWrite(LED0, HIGH);  
}

void loop() {
  delay(100);
  RCCHECK(rclc_executor_spin_some(&executor, RCL_MS_TO_NS(100)));
}